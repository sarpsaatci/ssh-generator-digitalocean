# doctl need to be installed
	# brew install doctl (Mac)
	# sudo snap install doctl (Linux supporting snap)

ssh-keygen -o -q -a 100 -t ecdsa -C dockerswarm@digitalocean -N "" -f ~/.ssh/id_ecdsa
ssh-add ~/.ssh/id_ecdsa
doctl compute ssh-key create terraform-deneme --public-key "$(cat ~/.ssh/id_ecdsa.pub)"
